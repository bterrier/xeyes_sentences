# xeyes sentences
*xeyes sentences* allow you to display the sentence you want using xeyes program.

![Hello world](https://gitlab.utc.fr/bterrier/xeyes_sentences/raw/master/img/Hello%20world.png)

## Prerequisites
You need *xeyes* program.
*xeyes* is part of the x11 suite of programs included in all linux distributions.

You also need *PyGTK* installed - required for screen resolution. `sudo apt-get install python-gtk2`.

## How to use it ?
Clone this project and run `./run.sh "your_sentence" [size]`

Parameters :
* your_sentence : [a-zA-Z!?]{0, 20}
* [optional] size : 25 by default

WARNING : the number of *xeyes* applications is limited, therefore, you can only print arount 20 characters at once. 

NB1 : you may need the execution right to run the script `chmod +x run.sh`

NB2 : to clear the sentence, just run `pkill xeyes`. Running the script clear screen at first.

## Work in progress...

Improvements to come :
* slide show mode (several sentences print)