import sys #args
import gtk #screen size

i_screen_width = gtk.gdk.screen_width()
i_screen_height = gtk.gdk.screen_height()


if(len(sys.argv) == 3):
    i_xeyes_size = int(sys.argv[2])
else:
    i_xeyes_size = 25

s_xeyes_command = "xeyes -geometry {0}x{0}-".format(i_xeyes_size)

i_margin = 100

global_x_pos = i_screen_width - i_margin
global_y_pos = i_margin

local_x_pos = 0
local_y_pos = 0

s_filename = "../xeyes_sentences.sh"
f_dataStream  = open(s_filename, "w")
