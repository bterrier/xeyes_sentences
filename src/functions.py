from alphabet import * #import alphabet
from resources import * #import resources

def initFile(s_sentences):
    f_dataStream.write("#Xeyes Sentences")
    f_dataStream.write("\n\n#Powered by Bastien T\n")
    f_dataStream.write("\n#Sentences : {}\n".format(s_sentences))
    f_dataStream.write("#Xeyes size : {}\n\n".format(i_xeyes_size))
##############################

def displayCharacter(char):

    f_dataStream.write("#Character {}\n".format(char))

    global global_x_pos
    global global_y_pos

    local_y_pos = global_y_pos

    for r in alphabet[char]:
        i_cptColum = 0
        local_x_pos = global_x_pos

        for c in r:
            #add command to file at good position
            if c == 1:
                f_dataStream.write("{}{}+{} & \n".format(s_xeyes_command, local_x_pos, local_y_pos))
            i_cptColum += 1
            local_x_pos = local_x_pos - i_xeyes_size

        local_y_pos = local_y_pos + i_xeyes_size


    global_x_pos = global_x_pos - (i_cptColum+1)*i_xeyes_size #+1 for space beetween characters

    #if end of line
    if global_x_pos < i_margin:
        global_y_pos += 6*i_xeyes_size #start new line
        global_x_pos = i_screen_width - i_margin #from the beginning

##############################

def displaySentence(sentence):

    global global_x_pos
    global global_y_pos

    global_x_pos = i_screen_width - i_margin
    global_y_pos = i_margin

    for char in sentence:
        if (ord(char) >= ord('A') and ord(char) <= ord('Z')) or char == "?" or char == "!" or char == " " :
            displayCharacter(char)
        else:
            print("Error, '{}' not a character".format(char))

    #beetween each sentence, system has to wait and then pkill xeyes
    f_dataStream.write("sleep 5\n")
    f_dataStream.write("pkill xeyes\n")
